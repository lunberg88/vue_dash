import store from '@/store'

export default {
  stackType(type = 0) {
    if(isNaN(type) !== true && typeof store.getters.getStack !== 'undefined') {
      if(store.getters.getStack !== null && typeof store.getters.getStack !== 'undefined') {
        if(store.getters.getStack.hasOwnProperty(type)) {
          return store.getters.getStack[type]
        } else {
          return 'Stack not found...'
        }
      }
    }
  },

  tagChips(tags) {
    let tag = tags.split(',');
    for(let i=0; i<tag.length; i++) {
      tag[i] = `<span class="badge badge-pill badge-teal"> ${tag[i]} </span>`;
    }
    tag = tag.join(' ');
    return tag;
  },

  showcurr(type) {
    if(store.getters.getCurrency !== null && typeof store.getters.getCurrency !== 'undefined') {
      if(store.getters.getCurrency.hasOwnProperty(type)) {
        return store.getters.getCurrency[type]
      } else {
        return 'Currency not found'
      }
    }
  },

  showStatus(type) {
    if(store.getters.getStatus !== null && store.getters.getStatus.hasOwnProperty(type)) {
      return statuses[type] + store.getters.getStatus[type];
    }
    return false;
  }

  /*checkStack(stack) {
    if(stack == 1) {
      return stack = 'Frontend';
    }
    else if(stack == 2) {
      return stack = 'Backend';
    }
    else if(stack == 3) {
      return stack = 'FullStack';
    }
    else if(stack == 4) {
      return stack = 'Mobile';
    }
    else if(stack == 5) {
      return stack = 'Design';
    }
    else if(stack == 6) {
      return stack = 'Traders';
    }
    else if(stack == 7) {
      return stack = 'DevOps';
    }
    else if(stack == 8) {
      return stack = 'Project Manager';
    }
    else if(stack == 9) {
      return stack = 'Product Manager';
    }
    else if(stack == 10) {
      return stack = 'Sales Manager';
    }
    else if(stack == 11) {
      return stack = 'CTO';
    }
  },*/
  /*
showcurr(currency) {
  if(currency == '1') {
    return '$';
  } else if(currency == '2') {
    return '€';
  } else if(currency == '3') {
    return '₽';
  } else if(currency == '4') {
    return '₴';
  }
},
*/
}

export const statuses = {
  0: '<span class="status--mark__default">●</span> ',
  1: '<span class="status--mark__warning">●</span> ',
  2: '<span class="status--mark__info">●</span> ',
  3: '<span class="status--mark__success">●</span> ',
  4: '<span class="status--mark__primary">●</span> ',
  5: '<span class="status--mark__danger">●</span> '
}

export const stackList = [
  {val: 1, text: 'Frontend'},
  {val: 2, text: 'Backend'},
  {val: 3, text: 'FullStack'},
  {val: 4, text: 'Mobile'},
  {val: 5, text: 'Design'},
  {val: 6, text: 'Traders'},
  {val: 7, text: 'DevOps'},
  {val: 8, text: 'Project Manager'},
  {val: 9, text: 'Product Manager'},
  {val: 10, text: 'Sales Manager'},
  {val: 11, text: 'CTO'}
]
export const currencyList = [
  {val: 1, txt: '$ USD'},
  {val: 2, txt: '€ EUR'},
  {val: 3, txt: '₴ HRN'},
  {val: 4, txt: '₽ RUB'}
]

export const dataTableHeaders = [
  {text: 'Full name', value: 'fio'},
  {text: 'Email', value: 'email'},
  {text: 'Stack', value: 'stack'},
  {text: 'Skills', value: 'tags'},
  {text: 'Status', value: 'status'},
  {text: 'Salary', value: 'salary'},
  {text: 'Action', value: ''}
]

export const itemsPerPage = [20,30,40,50]
