export default {
  checkType(val) {
    if(val == 1) {
      return 'badge badge-success';
    } else if(val == 2) {
      return 'badge badge-info';
    } else if(val == 3) {
      return 'badge badge-danger';
    } else {
      return 'badge badge-default';
    }
  },
  checkBadge(val) {
    if(val == 1) {
      return 'created';
    } else if(val == 2) {
      return 'updated';
    } else if(val == 3) {
      return 'deleted';
    } else {
      return 'system';
    }
  },
  formatDate(time) {
    let newTime = time.split(' ')
    return (newTime[0].replace('-', '.').replace('-', '.'))
  }
}

export const tableHeaders = [
  {text: 'Type', sortable: false, value: 'type'},
  {text: 'Actions', sortable: false, value: 'actions'},
  {text: 'Time', sortable: false, value: 'time'},
]
