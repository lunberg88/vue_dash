const host = 'http://lv-backend/api';

export const headers = { headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('access_token')
    }
}

export const api = {
    login: host + '/auth/login',
    logout: host + '/admin/logout',
    refreshToken: host + '/auth/refresh/token',
    getFullProfile: host + '/admin/profile',
    getPermission: host + '/admin/permission',
    getDashStacks: host + '/admin/service/stacks',
    getDashCurrency: host + '/admin/service/currency',
    updateProfile: host + '/profile/update',
    getServiceData: host + '/admin/service/getServiceData',
    getAllCandidates: host + '/admin/get_candidates',
    getCandidatePaginatePage: host + '/admin/get_candidates?page=',
    uploadCandidateCvs: host + '/admin/candidates/upload/cvs',
    deleteCandidateCvs: host + '/admin/candidates/remove/cvs',
    pushNewCandidate: host + '/admin/candidate/create/new',
    updateCandidateProfile: host + '/admin/candidate/edit/',
    getCandidateProfile: host + '/admin/candidate/view/',
    getHistoryList: host + '/admin/history',
    getHistoryPaginatePage: host + '/admin/history?page=',
    getAllOpenings: host + '/admin/openings/all',
    getAllMessages: host + '/admin/messages/all',
    getAllListOpenings: host + '/admin/openings/list/all',
}
