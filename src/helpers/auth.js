export default {
    getToken() {
        return localStorage.getItem('access_token')
    },
    getExpiresIn() {
        return localStorage.getItem('expires_in')
    },
    getRefreshToken() {
      return localStorage.getItem('refresh_token') || null
    }
}
