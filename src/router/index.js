import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    components: {default: () => import(/* webpackChunkName: "dash" */'@/views/Dash')},
    meta: {
      requiresAuth: true,
      requiresAdmin: true
    },
    children: [
      {
        path: '',
        redirect: '/dash'
      },
      {
        name: 'home',
        path: 'dash',
        component: () => import(/* webpackChunkName: "home" */'@/components/Homepage')
      },
      {
        name: 'openings',
        path: 'dash/openings',
        component: () => import(/* webpackChunkName: "openings" */'@/components/Openings/Openings')
      },
      {
        name: 'candidates',
        path: 'dash/candidates',
        component: () => import(/* webpackChunkName: "candidates" */'@/components/Candidates/Candidates')
      },
      {
        name: 'newcandidate',
        path: 'dash/candidate/add',
        component: () => import(/* webpackChunkName: "addCandidates" */'@/components/Candidates/AddCandidate'),
        meta: {
          props: true,
        }
      },
      {
        name: 'editprofile',
        path: 'dash/candidates/edit/:id',
        component: () => import(/* webpackChunkName: "candidateEditProfile" */'@/components/Candidates/EditProfile'),
        meta: {
          props: true
        },
        props: true
      },
      {
        name: 'candidate-profile',
        path: 'dash/candidates/candidate/:id',
        component: () => import(/* webpackChunkName: "fullProfile" */'@/components/Candidates/FullProfile'),
        meta: {
          props: true
        }
      },
      {
        name: 'candidate-newsletter',
        path: 'dash/newsletter',
        component: () => import(/* webpackChunkName: "candidateNewsletter" */'@/components/OpeningsMailer/CandidateNewsletter'),
      },
      {
        name: 'messages',
        path: 'dash/messages',
        component: () => import(/* webpackChunkName: "messages" */'@/components/Messages/Messages')
      },
      {
        name: 'history',
        path: 'dash/history',
        component: () => import(/* webpackChunkName: "history" */'@/components/History')
      },
      {
        name: 'profile',
        path: 'dash/profile',
        component: () => import(/* webpackChunkName: "profile" */'@/components/User/Profile'),
      },
      {
        name: 'calendar',
        path: 'dash/calendar',
        component: () => import(/* webpackChunkName: "calendar" */'@/components/Calendar/Calendar')
      },
      {
        name: 'notes',
        path: 'dash/notes',
        component: () => import(/* webpackChunkName: "notes" */'@/components/Notes/Notes')
      },
    ]
  },
  {
    path: '/',
    components: {default: () => import(/* webpackChunkName: "auth" */'@/views/Auth')},
    children: [
      {
        name: 'login',
        path: '/login',
        component: () => import(/* webpackChunkName: "loginPage" */'@/components/Login')
      }
    ]
  }
]

const router = new VueRouter({
  routes
})

export default router
