import store from '@/store'
import authService from '@/helpers/auth'
import {CHECK_AUTH, GET_PERMISSIONS, CLEAR_AUTH, REFRESH_LOGIN} from '@/store/actions.type'
import {FETCH_SERVICE_DATA} from '@/store/actions.type'

export function checkAccessMiddleware(to, from, next) {
  const isAuth = authService.getToken()
  const refreshToken = authService.getRefreshToken()
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth)
  const requiresAdmin = to.matched.some(record => record.meta.requiresAdmin)
  const current_time = Math.floor(Date.now() / 1000)

  /*else if(authService.getExpiresIn() < current_time && to.path !== '/login') {
  console.log('token is expired and refresh NULL')
  next({name: 'login'})
}*/

  if(requiresAuth && requiresAdmin && !isAuth) {
    next({name: 'login'});
  } else if(!isAuth && to.path === '/login') {
    next();
  } else if(isAuth && refreshToken === '' || isAuth && refreshToken === null) {
    store.dispatch(CLEAR_AUTH)
    next({name: 'login'})
  } else {
    //todo if token exist, let's check it for expire...
    if(authService.getExpiresIn() > current_time) {
      next()
      return Promise.all([
        store.dispatch(CHECK_AUTH),
        store.dispatch(GET_PERMISSIONS),
        store.dispatch(FETCH_SERVICE_DATA),
      ]).then(() => next())
    } else {
      //todo if refreshToken exist && not NULL, trying to refresh it...
      if(refreshToken !== null || refreshToken !== '' || refreshToken !== undefined) {
        return Promise.all([
          store.dispatch(REFRESH_LOGIN, {refresh_token: refreshToken})
        ])
      } else {
        //todo if token doesn't exist or NULL, get out...
        store.dispatch(CLEAR_AUTH)
        next({name: 'login'})
      }
      next()
    }
  }
}
