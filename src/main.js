import Vue from 'vue'
import App from '@/App.vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.css'

import DaySpanVuetify from 'dayspan-vuetify'
import 'dayspan-vuetify/dist/lib/dayspan-vuetify.min.css'

import store from '@/store'
import router from '@/router'
import {CHECK_AUTH, GET_PERMISSIONS} from "@/store/actions.type"

import axios from 'axios'
import {checkAccessMiddleware} from "./router/middleware";

Vue.use(Vuetify);

import VueApexCharts from 'vue-apexcharts'

Vue.use(VueApexCharts)
Vue.use(DaySpanVuetify, {
  methods: {
    getDefaultEventColor: () => '#1976d2'
  }
})

//Request interceptor
axios.interceptors.request.use(function(config) {
    const token = localStorage.getItem('access_token')
    if(token) {
      config.headers.Authorization = `Bearer ${token}`
    }
    return config
}, function(err) {
    return Promise.reject(err)
})


//Response interceptor
axios.interceptors.response.use(function (response) {
  return response
}, function (error) {
  return Promise.reject(error)
})

router.beforeEach(checkAccessMiddleware)

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
