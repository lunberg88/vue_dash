export const LOADING = 'loading'
export const LOADING_MENU = 'loadingMenu'
export const LOGIN = 'login'
export const REFRESH_LOGIN = 'refresh_login'
export const CLEAR_AUTH = 'clearAuth'

export const FETCH_SERVICE_DATA = 'fetchServiceData'

export const LOGOUT = 'logout'
export const CHECK_AUTH = 'checkAuth'
export const GET_PERMISSIONS = 'getPermissions'

export const LAST_OPENINGS_HP = 'lastOpeningsHP'
export const FETCH_OPENINGS_ALL = 'fetchOpeningsAll'
export const OPENING_DETAIL = 'openingDetail'
export const GET_LIST_OPENING = 'getListOpenings'

export const UPDATE_PROFILE = 'updateProfile'

export const GET_ALL_CANDIDATES = 'getAllCandidates'
export const GET_CURRENT_CANDIDATE = 'getCurrentCandidate'
export const UPDATE_CANDIDATE_INFO = 'updateCandidateInfo'
export const SET_PAGINATE = 'setPaginate'
export const REFRESH_CANDIDATE_LIST = 'refreshCandidateList'

export const GET_HISTORY_LIST = 'getHistoryList'
export const SET_PAGINATE_HISTORY = 'setPaginateHistory'

export const GET_MESSAGES_LIST = 'getMessagesListAll'

export const ADD_CANDIDATE = 'addCandidate'
