export const IS_LOADING = 'isLoading'
export const IS_LOADING_MENU = 'isLoadingMenu'
export const LOGIN_USER = 'loginUser'
export const SET_ERRORS = 'setErrors'

export const SET_SERVICE_DATA = 'setServiceData'

export const SET_PERMISSION = 'setPermission'
export const SET_AUTH  = 'setAuth'
export const PURGE_USER = 'purgeUser'

export const SET_STACKS = 'setStacks'
export const SET_CURRENCY = 'setCurrency'

export const FETCH_LAST_OPNINGS = 'fetchLastOpenings'
export const FETCH_ALL_OPENINGS = 'fetchAllOpenings'
export const VIEW_OPENING_DETAIL = 'viewOpeningDetail'
export const FETCH_LIST_OPENINGS = 'fetchListOpenings'

export const SET_SNACK_BAR = 'setSnackBar'

export const FETCH_CANDIDATES_LIST = 'fetchCandidatesList'
export const FETCH_HISTORY_LIST = 'fetchHistoryList'
export const FETCH_MESSAGES_LIST = 'fetchMessagesList'
export const FETCH_CURRENT_CANDIDATE = 'fetchCurrentCandidate'
