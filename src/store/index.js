import Vue from 'vue'
import Vuex from 'vuex'

import auth from './modules/auth'
import main from './modules/main'
import openings from './modules/openings'
import candidates from './modules/candidates'
import history from './modules/history'
import messages from './modules/messages'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
      auth,
      main,
      openings,
      candidates,
      history,
      messages,
    }
})
