import {
  IS_LOADING,
  FETCH_CANDIDATES_LIST,
  SET_SNACK_BAR,
  FETCH_CURRENT_CANDIDATE
} from '../mutations.type'
import {
  GET_ALL_CANDIDATES,
  ADD_CANDIDATE,
  SET_PAGINATE,
  GET_CURRENT_CANDIDATE, UPDATE_CANDIDATE_INFO, REFRESH_CANDIDATE_LIST
} from '../actions.type'
import { api, headers } from '@/helpers/api'
import axios from 'axios'

const state = {
  candidates: {},
  candidate: null,
}

const getters = {
  getCandidatesList: state => {
    return state.candidates
  },
  getCurrentCandidate: state => {
    return state.candidate
  },
}

const mutations = {
  [FETCH_CANDIDATES_LIST] (state, payload) {
    state.candidates = payload.data
  },
  [FETCH_CURRENT_CANDIDATE] (state, payload) {
    state.candidate = payload.data
  }
}

const actions = {
  [GET_ALL_CANDIDATES] (context) {
    return new Promise((resolve, reject) => {
      context.commit(IS_LOADING)
      axios.get(api.getAllCandidates, headers)
        .then(res => {
          context.commit(FETCH_CANDIDATES_LIST, res.data)
          context.commit(IS_LOADING)
        })
        .catch(e => {
          console.log('Error: ' + e.status)
          reject(e)
          context.commit(IS_LOADING)
        })
    })
  },
  [ADD_CANDIDATE] (context, payload) {
    return new Promise((resolve, reject) => {
      context.commit(IS_LOADING)
      axios({
        method: 'POST',
        url: api.pushNewCandidate,
        data: payload,
        headers
      }).then(res => {
        context.commit(IS_LOADING)
        resolve(res.data)
        context.commit(SET_SNACK_BAR, res.status)
      })
        .catch(e => {
          context.commit(IS_LOADING)
          reject(e.response.status)
          context.commit(SET_SNACK_BAR, e.status)
          console.log('Error add new candidate: ' + e.response.status)
        })
    })
  },
  [UPDATE_CANDIDATE_INFO] (context, payload) {
    context.commit(IS_LOADING)
    payload.tags = payload.tags.join(',')
    return new Promise(() => {
      axios({
        method: 'PUT',
        url: api.updateCandidateProfile + payload.id,
        data: payload,
        headers
      })
        .then(res => {
          context.commit(IS_LOADING)
          console.log(res.data)
        })
        .catch(e => {
          context.commit(IS_LOADING)
          console.log(e.response.status)
        })
    })
  },
  [SET_PAGINATE] (context, payload) {
    return new Promise((resolve, reject) => {
      context.commit(IS_LOADING)
      axios.get(api.getCandidatePaginatePage + payload.page, headers)
        .then(res => {
          context.commit(IS_LOADING)
          resolve(res.data)
          context.commit(FETCH_CANDIDATES_LIST, res.data)
        })
        .catch(e => {
          context.commit(IS_LOADING)
          reject(e.response)
          if(e.response) {
            console.log('Error pagination: ' + e.response.data.message + ', code: ' + e.response.status)
          }
        })
    })
  },
  [GET_CURRENT_CANDIDATE] (context, payload) {
    return new Promise((resolve, reject) => {
      context.commit(IS_LOADING)
      axios.get(api.getCandidateProfile + payload, headers)
        .then(res => {
          context.commit(IS_LOADING)
          context.commit(FETCH_CURRENT_CANDIDATE, res.data)
          resolve(res.data)
        })
        .catch(e => {
          reject(e.response)
          console.log(e.response.status)
        })
    })
  },
  [REFRESH_CANDIDATE_LIST] (context) {
    return new Promise(() => {
      axios.get(api.getAllCandidates, headers)
        .then(res => {
          context.commit(FETCH_CANDIDATES_LIST, res.data)
        })
        .catch(e => {
          console.log(e.response.status)
        })
    })
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}

