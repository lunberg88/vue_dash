import axios from 'axios'
import { api, headers } from '@/helpers/api'
import {IS_LOADING, IS_LOADING_MENU, SET_SERVICE_DATA} from '../mutations.type'
import {FETCH_SERVICE_DATA, LOADING, LOADING_MENU} from '../actions.type'

const state = {
    isLoading: false,
    isLoadingMenu: false,
    serviceData: false,
}

const getters = {
    isLoadingStatus: state => {
        return state.isLoading
    },
    isLoadingMenuStatus: state => {
        return state.isLoadingMenu
    },
    getStack: state => {
      return state.serviceData.stacks
    },
    getCurrency: state => {
      return state.serviceData.currencies
    },
    getStatus: state => {
      return state.serviceData.statuses
    },
}

const mutations = {
    [IS_LOADING] (state) {
        state.isLoading = !state.isLoading
    },
    [IS_LOADING_MENU] (state) {
        state.isLoadingMenu = !state.isLoadingMenu
    },
    [SET_SERVICE_DATA] (state, payload) {
      state.serviceData = payload.data
    }
}

const actions = {
    [LOADING] ({commit}) {
        commit(IS_LOADING)
    },
    [LOADING_MENU] ({commit}) {
        commit(IS_LOADING_MENU)
    },
    [FETCH_SERVICE_DATA] (context) {
        context.commit(IS_LOADING)
        return new Promise(() => {
          axios.get(api.getServiceData, headers)
            .then(res => {
              context.commit(IS_LOADING)
              context.commit(SET_SERVICE_DATA, res.data)
            })
            .catch(e => {
              context.commit(IS_LOADING)
              console.log(e.response.status)
            })
        })
    }
}

export default {
    state,
    getters,
    mutations,
    actions
}
