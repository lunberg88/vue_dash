import axios from 'axios'
import { api } from '../../helpers/api'

import {
  IS_LOADING,
  FETCH_LAST_OPNINGS,
  FETCH_ALL_OPENINGS,
  VIEW_OPENING_DETAIL,
  FETCH_LIST_OPENINGS
} from "../mutations.type"
import {LAST_OPENINGS_HP, FETCH_OPENINGS_ALL, OPENING_DETAIL, GET_LIST_OPENING} from "../actions.type"

const state = {
    lastOpening: [],
    openingsList: {},
    openingDetail: [],
    listOpenings: [],
}

const getters = {
    getLastOpenings(state) {
        return state.lastOpening
    },
    getAllOpenings(state) {
        return state.openingsList
    },
    getOpeningDetail(state) {
        return state.openingDetail
    },
    getListOpenings(state) {
      return state.listOpenings
    }
}

const mutations = {
    [FETCH_LAST_OPNINGS] (state, payload) {
        state.lastOpening = payload
    },
    [FETCH_ALL_OPENINGS] (state, payload) {
        state.openingsList = payload.data
    },
    [VIEW_OPENING_DETAIL] (state, payload) {
        state.openingDetail = payload
    },
    [FETCH_LIST_OPENINGS] (state, payload) {
        state.listOpenings = payload
    }
}

const actions = {
    [LAST_OPENINGS_HP] (context) {
        return new Promise((resolve) => {
            context.commit(IS_LOADING)
            axios.get(api.getLastOpeningsHp)
                .then(res => {
                    context.commit(FETCH_LAST_OPNINGS, res.data.data)
                    resolve(res.data.data)
                    context.commit(IS_LOADING)
                })
                .catch(e => {
                    context.commit(IS_LOADING)
                    console.log('error fetching openings: ' + e.statusMessage)
                })
        })
    },
    [FETCH_OPENINGS_ALL] (context) {
        return new Promise((resolve) => {
            context.commit(IS_LOADING)
            axios.get(api.getAllOpenings)
                .then(res => {
                    context.commit(FETCH_ALL_OPENINGS, res.data)
                    resolve(res.data)
                    context.commit(IS_LOADING)
                })
                .catch(e => {
                    context.commit(IS_LOADING)
                    console.log('error fetching all openings: ' + e.statusMessage)
                })
        })
    },
    [OPENING_DETAIL] (context, payload) {
        return new Promise((resolve, reject) => {
            context.commit(IS_LOADING)
            axios.get(api.viewOpeningDetail + payload)
                .then(res => {
                    context.commit(VIEW_OPENING_DETAIL, res.data)
                    context.commit(IS_LOADING)
                    resolve(res.data)
                })
                .catch(e => {
                    context.commit(IS_LOADING)
                    console.log('error fetching opening detail: ' + e.statusMessage)
                    reject(e.statusMessage)
                })
        })
    },
    [GET_LIST_OPENING] (context) {
      context.commit(IS_LOADING)
      return new Promise(() => {
        axios.get(api.getAllListOpenings)
          .then(res => {
            context.commit(IS_LOADING)
            context.commit(FETCH_LIST_OPENINGS, res.data)
          })
          .catch(e => {
            context.commit(IS_LOADING)
            console.log(e.response.status)
          })
      })
    }
}

export default {
    state,
    getters,
    mutations,
    actions
}
