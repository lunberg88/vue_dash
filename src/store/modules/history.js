import {FETCH_HISTORY_LIST, IS_LOADING} from "../mutations.type";
import {GET_HISTORY_LIST, SET_PAGINATE_HISTORY} from "../actions.type";
import axios from 'axios'
import { api, headers } from "../../helpers/api";

const state = {
  historyList: []
}

const getters = {
  getHistoryList: state => {
    return state.historyList
  }
}

const mutations = {
  [FETCH_HISTORY_LIST] (state, payload) {
    state.historyList = payload.data
  }
}

const actions = {
  [GET_HISTORY_LIST] (context) {
    return new Promise((resolve, reject) => {
      context.commit(IS_LOADING)
      axios.get(api.getHistoryList, headers)
        .then(res => {
          context.commit(FETCH_HISTORY_LIST, res.data)
          resolve(res.data)
          context.commit(IS_LOADING)
        })
        .catch(e => {
          reject(e.status)
          console.log('Error: ' + e.status)
          context.commit(IS_LOADING)
        })
    })
  },
  [SET_PAGINATE_HISTORY] (context, payload) {
    return new Promise((resolve, reject) => {
      context.commit(IS_LOADING)
      axios.get(api.getHistoryPaginatePage + payload.page, headers)
        .then(res => {
          context.commit(IS_LOADING)
          resolve(res.data)
          context.commit(FETCH_HISTORY_LIST, res.data)
        })
        .catch(e => {
          context.commit(IS_LOADING)
          reject(e.response)
          if(e.response) {
            console.log('Error pagination: ' + e.response.data.message + ', code: ' + e.response.status)
          }
        })
    })
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
