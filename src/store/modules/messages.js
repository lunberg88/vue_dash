import axios from 'axios'
import {FETCH_MESSAGES_LIST, IS_LOADING} from "../mutations.type";
import {GET_MESSAGES_LIST} from "../actions.type";
import {api, headers} from "../../helpers/api";

const state = {
  messages: []
}

const getters = {
  getMessagesList(state) {
    return state.messages
  }
}

const mutations = {
  [FETCH_MESSAGES_LIST] (state, payload) {
    state.messages = payload.data
  }
}

const actions = {
  [GET_MESSAGES_LIST] (context) {
    return new Promise((resolve, reject) => {
      context.commit(IS_LOADING)
      axios.get(api.getAllMessages, headers)
        .then(res => {
          resolve(res.data)
          context.commit(IS_LOADING)
          context.commit(FETCH_MESSAGES_LIST, res.data)
        })
        .catch(e => {
          reject(e.status)
          context.commit(IS_LOADING)
        })
    })
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
