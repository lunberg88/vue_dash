import axios from 'axios'
import router from '../../router'
import { api, headers } from '../../helpers/api'
import {
  LOGIN_USER,
  SET_AUTH,
  IS_LOADING,
  PURGE_USER,
  SET_UPDATE_PROFILE,
  SET_SNACK_BAR,
  SET_PERMISSION,
  SET_ERRORS
} from "../mutations.type";
import {
  LOGIN,
  LOGOUT,
  UPDATE_PROFILE,
  CHECK_AUTH,
  GET_PERMISSIONS,
  LOADING_MENU,
  REFRESH_LOGIN, CLEAR_AUTH, GET_ALL_CANDIDATES, GET_LIST_OPENING,
} from "../actions.type";
import authService from '@/helpers/auth'

const state = {
    userInfo: {
        access_token: localStorage.getItem('access_token') || null,
        expires_in: localStorage.getItem('expires_in') || null,
        refresh_token: localStorage.getItem('refresh_token') || null,
        profile: {},
        permission: null,
    },
    errors: null,
    snackBar: {
        state: null,
        type: null,
        color: null,
        msg: null
    }
}

const getters = {
    user: state => {
        return state.userInfo
    },
    userProfile: state => {
        return state.userInfo.profile
    },
    userRole: state => {
        return state.userInfo.permission
    },
    userAuth: state => {
        if(state.userInfo.access_token != null) {
            return true
        }
        return false
    },
    getSnackBar: state => {
        return state.snackBar
    }
}

const mutations = {
    [LOGIN_USER] (state, payload){
        state.userInfo.access_token = payload.access_token
        state.userInfo.refresh_token = payload.refresh_token
        state.userInfo.expires_in = ((payload.expires_in + (Math.floor(Date.now() / 1000))))
        localStorage.setItem('access_token', payload.access_token)
        localStorage.setItem('refresh_token', payload.refresh_token)
        localStorage.setItem('expires_in', ((payload.expires_in + (Math.floor(Date.now() / 1000)))))
    },
    [SET_AUTH] (state, payload) {
        state.userInfo.profile = payload
    },
    [SET_PERMISSION] (state, payload) {
        state.userInfo.permission = payload
    },
    [PURGE_USER] (state) {
        state.userInfo.access_token = null
        state.userInfo.expires_in = null
        state.userInfo.refresh_token = null
        state.userInfo.profile = null
        localStorage.removeItem('access_token')
        localStorage.removeItem('expires_in')
        localStorage.removeItem('refresh_token')
    },
    [SET_SNACK_BAR] (state, payload) {
        state.snackBar.state = true
        state.snackBar.type = payload
    },
    [SET_ERRORS] (state, payload) {
      state.errors = payload
    }
}

const actions = {
    [LOGIN] (context, payload) {
        const data = {
            username: payload.username,
            password: payload.password,
        }
        return new Promise((resolve, reject) => {
            context.commit(IS_LOADING)
            axios.post(api.login, data)
                .then(res => {
                    context.commit(LOGIN_USER, {
                        access_token: res.data.access_token,
                        expires_in: res.data.expires_in,
                        refresh_token: res.data.refresh_token
                    })
                    resolve(res.data)
                    context.commit(IS_LOADING)
                    router.push('/')
                })
                .catch(e => {
                    reject(e.response)
                    context.commit(IS_LOADING)
                    console.log('error: ' + e.statusMessage)
                })
        }).then(() => {
            axios.get(api.getFullProfile, headers)
                .then(res => {
                    context.commit(SET_AUTH, res.data)
                })
                .catch(e => {
                    console.log('Profile error: ' + e.response.data.message)
                })
        }).then(() => {
          axios.get(api.getPermission, headers)
            .then(res => {
              context.commit(SET_PERMISSION, res.data)
            })
            .catch(e => {
              console.log('Permission error... ' + e.response.data.message + ', ' + e.response.status)
            })
        })
    },
    [REFRESH_LOGIN] (context, payload) {
    const data = {
      refresh_token: payload.refresh_token
    }
    return new Promise((resolve) => {
      context.commit(IS_LOADING)
      axios.post(api.refreshToken, data)
        .then(res => {
          context.commit(LOGIN_USER, {
            access_token: res.data.access_token,
            expires_in: res.data.expires_in,
            refresh_token: res.data.refresh_token
          })
          resolve(res.data)
          context.commit(IS_LOADING)
          router.push('/')
        })
        .catch(e => {
          context.commit(IS_LOADING)
          console.log('error: ' + e.statusMessage)
        })
    }).then(() => {
      axios.get(api.getFullProfile, headers)
        .then(res => {
          context.commit(SET_AUTH, res.data)
        })
        .catch(e => {
          console.log('Profile error: ' + e.statusMessage)
        })
    }).then(() => {
      axios.get(api.getPermission, headers)
        .then(res => {
          context.commit(SET_PERMISSION, res.data)
          resolve(res.data)
          console.log('permission: ' + res.data)
        })
        .catch(e => {
          console.log('Permission error... ' + e.statusMessage + ', ' + e.statusText)
        })
    })
  },
    [LOGOUT] (context) {
        return new Promise((resolve, reject) => {
            context.commit(IS_LOADING)
            axios.post(api.logout, headers)
              .then(res => {
                resolve(res)
                context.commit(IS_LOADING)
                context.commit(PURGE_USER)
                router.push('/login')
              })
              .catch(e => {
                if(e.response) {
                  reject(e.response)
                  context.commit(IS_LOADING)
                  context.commit(PURGE_USER)
                  router.push('/login')
                }
              })
        })
    },
    [UPDATE_PROFILE] (context, payload) {
        return new Promise((resolve, reject) => {
            context.commit(IS_LOADING)
            axios({
             method: 'POST',
             url: api.updateProfile,
             data: payload,
             headers
            })
                .then(res => {
                    context.commit(IS_LOADING)
                    console.log(res.data.message)
                    resolve(res.data.message)
                    context.commit(SET_SNACK_BAR, res.status)
                })
                .catch(e => {
                    context.commit(IS_LOADING)
                    console.log(e.statusMessage)
                    reject(e.statusMessage)
                    context.commit(SET_SNACK_BAR, res.status)
                })
        }).then(() => {
            axios.get(api.getFullProfile, headers)
                .then(res => {
                    context.commit(SET_AUTH, res.data)
                })
                .catch(e => {
                    console.log('Error updating profile: ' + e.statusMessage)
                })
        })
    },
    [GET_PERMISSIONS] (context) {
      if(localStorage.getItem('access_token') == null) return;
      return new Promise((reject) => {
        context.dispatch(LOADING_MENU)
        axios.get(api.getPermission, headers)
          .then(res => {
            context.commit(SET_PERMISSION, res.data)
            context.dispatch(LOADING_MENU)
          })
          .catch(e => {
            reject(e.response)
            if(e.response.status == 401) {
              context.dispatch(LOADING_MENU)
            }
          })
      })
    },
    [CHECK_AUTH] (context) {
      if(authService.getToken() !== undefined || authService.getToken() !== null) {
        return new Promise(() => {
          axios.get(api.getFullProfile, headers)
            .then(res => {
              context.commit(SET_AUTH, res.data)
            })
            .catch(e => {
              context.commit(SET_ERRORS, e.response.data)
              if(e.response && e.response.status == 401) {
                context.commit(PURGE_USER)
              }
            })
        })
      } else {
        context.commit(PURGE_USER)
        router.push({name: 'login'})
      }
    },
    [CLEAR_AUTH] (context) {
      return new Promise(() => {
        context.commit(PURGE_USER)
      })
    }
}

export default {
    mutations,
    actions,
    state,
    getters
}
